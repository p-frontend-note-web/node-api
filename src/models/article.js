const mongoose = require('mongoose');

const articleSourceSchema = new mongoose.Schema({
  label: { type: String, required: true },
  link: { type: String, required: true },
});

const articlePreviewSchema = new mongoose.Schema({
  desc: { type: String, required: true },
  image: { type: String },
});

const articleSchema = new mongoose.Schema({
  title: { type: String, required: true },
  content: { type: String, required: true },
  category: { type: String, required: true },
  url: { type: String, required: true },
  series: { type: String, default: '' },
  sources: { type: [articleSourceSchema], default: [] },
  tags: { type: [String], default: [] },
  status: { type: Number, required: true },
  createdTime: { type: Date, default: Date.now },
  updatedTime: { type: Date, default: Date.now },
  preview: { type: articlePreviewSchema },
});

module.exports = mongoose.model('Article', articleSchema);
