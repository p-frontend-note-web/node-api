const greeting = () => 'bye';

async function hi() {
  const res = await greeting();
  return res;
}
