const mongoose = require('mongoose');

const categorySchema = new mongoose.Schema({
  name: { type: String, required: true },
  id: { type: String, required: true },
  createdTime: { type: Date, default: Date.now },
  updatedTime: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Category', categorySchema);
