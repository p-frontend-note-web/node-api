function errMsg(errCode) {
  const errMsgCollection = {
    1000: '未知錯誤',
    1001: '參數錯誤',
    1002: '資料庫錯誤',

    // user
    2001: '此使用者已存在',
    2002: '帳號或密碼錯誤',

    // 分類
    3001: '此分類已存在',
    3002: '此分類不存在',

    // 子分類
    4001: '此子分類已存在該分類中',
    4002: '此分類不存在',

    // 文章
    5001: '查無此文章',

    // auth
    7001: '此權限已存在',
    7002: '此權限不存在',

    // 其他
    8001: '資料庫無符合條件之資料',

    // 權限控制
    9001: '驗證無效',
    9002: '無此操作權限',
    9003: '無權限操作管理員',
  };

  const code = String(errCode);
  const isErrCodeExist = Object.keys(errMsgCollection).includes(code);
  if (!isErrCodeExist) {
    return '未知錯誤';
  }

  return errMsgCollection[code];
}

module.exports = errMsg;
