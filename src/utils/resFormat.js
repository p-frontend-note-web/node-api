// 回應非200時
function resError(ctx) {
  return (message = {}, status = 400) => {
    ctx.body = {
      message,
    };
    ctx.status = status;
  };
}

// 回應200時
function resOk(ctx) {
  return (data = {}, message = '', status = 200) => {
    const resBody = {
      ...data,
    };

    if (message) {
      resBody.message = '';
    }

    ctx.body = data;
    ctx.status = status;
  };
}

// 回應200且有頁數時
function resOkWithPage(items, pageInfo, summary) {
  return {
    code: 0,
    content: items,
    pageInfo,
    summary,
  };
}

// 回應200但沒資料時
function resOkWithZeroData(pageInfo, warning, summary) {
  return {
    code: 0,
    content: [],
    pageInfo,
    summary,
    warning,
  };
}

exports.resError = resError;
exports.resOk = resOk;
exports.resOkWithPage = resOkWithPage;
exports.resOkWithZeroData = resOkWithZeroData;
