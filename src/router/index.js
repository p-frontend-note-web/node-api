const Router = require('koa-router');
const router = new Router();
const ctrls = require('../controllers');

router.get('/', (ctx) => ctrls.root(ctx));

// article
router.get('/article', (ctx) => ctrls.getArticles(ctx));
router.get('/article/:url', (ctx) => ctrls.getArticleByUrl(ctx));

module.exports = router;
