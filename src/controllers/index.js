const root = require('./root/root');

const getArticles = require('./article/getArticles');
const getArticleByUrl = require('./article/getArticleByUrl');

module.exports = {
  root,
  getArticleByUrl,
  getArticles,
};
