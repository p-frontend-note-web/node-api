const { resError, resOk } = require('../../utils/resFormat');
const Article = require('../../models/article');

const generatePageInfo = (pageIndex, pageSize, documentLength) => {
  return {
    pageIndex: +pageIndex,
    pageSize: +pageSize,
    totalCounts: documentLength,
    totalPage: Math.ceil(documentLength / +pageSize),
  };
};

const deleteContent = (articleList) => {
  return articleList.map((item) => {
    const obj = item.toObject();
    delete obj.content;
    return obj;
  });
};

const queryPageInfo = async (countDocQuery, query) => {
  const documentLength = await Article.countDocuments(countDocQuery);
  const pageInfo = generatePageInfo(+query.pageIndex, +query.pageSize, documentLength);

  return pageInfo;
};

const queryDb = async (query) => {
  const searchConditon = {
    async all() {
      const searchCondition = { status: 1 };
      const articleList = await Article.find(searchCondition)
        .limit(+query.pageSize)
        .skip((+query.pageIndex - 1) * +query.pageSize)
        .sort({ createdTime: -1 });

      const pageInfo = await queryPageInfo(searchCondition, query);

      return {
        articleList: deleteContent(articleList),
        pageInfo,
      };
    },
    async category() {
      const searchCondition = {
        category: query.category,
        status: 1,
      };

      const articleList = await Article.find(searchCondition)
        .limit(+query.pageSize)
        .skip((+query.pageIndex - 1) * +query.pageSize)
        .sort({ createdTime: -1 });

      const pageInfo = await queryPageInfo(searchCondition, query);

      return {
        articleList: deleteContent(articleList),
        pageInfo,
      };
    },
    async keyword() {
      const searchCondition = {
        title: { $regex: query.keyword, $options: 'i' },
        status: 1,
      };

      const articleList = await Article.find(searchCondition)
        .limit(+query.pageSize)
        .skip((+query.pageIndex - 1) * +query.pageSize)
        .sort({ createdTime: -1 });

      const pageInfo = await queryPageInfo(searchCondition, query);

      return {
        articleList: deleteContent(articleList),
        pageInfo,
      };
    },
    async series() {
      const searchCondition = {
        series: query.series,
        status: 1,
      };
      const articleList = await Article.find(searchCondition)
        .limit(+query.pageSize)
        .skip((+query.pageIndex - 1) * +query.pageSize)
        .sort({ createdTime: -1 });

      const pageInfo = await queryPageInfo(searchCondition, query);

      return {
        articleList: deleteContent(articleList),
        pageInfo,
      };
    },
    async latest() {
      const searchCondition = { status: 1 };
      const articleList = await Article.find(searchCondition)
        .limit(+query.pageSize)
        .skip((+query.pageIndex - 1) * +query.pageSize)
        .sort({ createdTime: -1 });
      const pageInfo = await queryPageInfo(searchCondition, query);

      return {
        articleList: deleteContent(articleList),
        pageInfo,
      };
    },
    async more() {
      const searchCondition = { status: 1 };
      const articleList = await Article.find(searchCondition).limit(5).skip(3).sort({ createdTime: -1 });
      const pageInfo = await queryPageInfo(searchCondition, query);

      return {
        articleList: deleteContent(articleList),
        pageInfo,
      };
    },
    async tag() {
      const searchCondition = { status: 1, tags: query.tag };
      const articleList = await Article.find(searchCondition).limit(5).sort({ createdTime: -1 });
      const pageInfo = await queryPageInfo(searchCondition, query);

      return {
        articleList: deleteContent(articleList),
        pageInfo,
      };
    },
    async latestAndmore() {
      let latestArticles = await Article.find({ status: 1 }).limit(3).sort({ createdTime: -1 });
      let moreArticles = await Article.find({ status: 1 }).limit(5).skip(3).sort({ createdTime: -1 });

      latestArticles = deleteContent(latestArticles);
      moreArticles = deleteContent(moreArticles);

      const articleList = {
        latestArticles,
        moreArticles,
      };

      return {
        articleList,
      };
    },
    async url() {
      const articleList = await Article.find({ status: 1 });

      // .filter((item => {
      //   return {
      //     aritlceName: item.url
      //   }
      // }));

      return {
        articleList: deleteContent(articleList),
      };
    },
  };

  const res = await searchConditon[query.type]();

  return res;
};

async function api(ctx) {
  try {
    const res = await queryDb(ctx.query);

    resOk(ctx)(res);
  } catch (e) {
    resError(ctx)();
  }
}

module.exports = api;
