const { resError, resOk } = require('../../utils/resFormat');
const Article = require('../../models/article');

async function api(ctx) {
  try {
    console.log('ctx.params.url', ctx.params.url);

    const articleResult = await Article.findOne({ url: ctx.params.url });

    resOk(ctx)(articleResult);
  } catch (e) {
    resError(ctx)();
  }
}

module.exports = api;
