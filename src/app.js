const Koa = require('koa');
const cors = require('@koa/cors');
const koaBody = require('koa-body');
const router = require('./router/index');
const mongoose = require('mongoose');
const path = require('path');
const dotenv = require('dotenv');
const envPath = process.env.NODE_ENV === 'prod' ? '../.env' : `../.env.${process.env.NODE_ENV}`;

dotenv.config({ path: path.resolve(__dirname, envPath) });

mongoose
  .connect(process.env.MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('connect to mongoDB successfully'))
  .catch((e) => console.log('connect fail', e));
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

require('dotenv').config();

const app = new Koa();
app.use(cors());
app.use(koaBody());
app.use(router.routes());

app.listen(process.env.USE_PORT);
