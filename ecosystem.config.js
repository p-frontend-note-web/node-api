module.exports = {
  apps: [
    {
      name: 'node-api',
      script: './src/app.js',
      watch: true,
      env: {
        NODE_ENV: 'prod',
      },
    },
  ],
};
