require('dotenv').config();

const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const WebpackBar = require('webpackbar');
const path = require('path');

const config = {
  entry: ['./src/app.js'],
  output: {
    path: path.resolve('dist'),
    filename: 'js/[name].[hash:8].js', // js ouput到dist資料夾的位置
    chunkFilename: 'js/[name].shared.js', // js ouput到dist資料夾的位置
  },
  module: {
    rules: [],
  },
  plugins: [new WebpackBar(), new CleanWebpackPlugin()],
  resolve: {
    alias: {
      '@': path.resolve('src/'),
    },
  },
};

module.exports = config;
