const Koa = require('koa');
const app = new Koa();
const koaBody = require('koa-body');
const router = require('./router/index');

app.use(koaBody());
app.use(router.routes());

const ctrls = require('./controllers');
console.log('ctrls===', ctrls);

app.listen(7076);
